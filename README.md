# Integração Contínua & Implantação Contínua

## Objetivo

O presente trabalho busca compreender a construção de pipeline para integração e implantação contínua. Utilizando
técnicas e visão de um modelo baseado em DevOps.

## Resumo

Testes unitários são essenciais para a construção de novos sistemas. Ajuda a validar cada módulo de unidade desenvolvido
ou a refatorar de forma continua. O mesmo não ocorre em ambientes de integração (quando há) e pré produção.

O próprio sistema quando desenvolvido, não é analisado e construido para ser testável e um dos efeitos é puramente
entropico, dado o aumento de novos elementos, temos um problema de escalabilidade organizacional.

A integração com componentes internos e externos é identificado como uma etapa necessária no processo de um sistema,
entretanto pouco é analisado sobre as medidas para garantir seus testes automatizados.

Cada desenvolvedor é obrigado a testar localmente cada uma das operações necessárias para a troca de mensagem. Dado que
no ambiente local muitas vezes não contém ferramentas, componentes ou agentes os quais facilitam a simulação em
produção.

## Ambientes

    - Integração
    - Pré Produção
    - Produção

![pipeline-ci-cd](imagens/pipeline.png)

![base-teorica-homologacao](imagens/teoria-pre-producao.png)

## Situações de testes em homologação

Vamos pensar na seguinte situação hipotetica. Seja serviço
"A" representado pela imagem abaixo

![servico-a](imagens/servico-a.png)

Tipo de mudança : contrato de API

---

Caso ocorra mudança de contrato, teremos as seguintes etapas

1. Testes não regressivos serão liberados (leia-se testar o módulo e suas dependências)
2. Testes regressivos serão feitos após a conclusão geral de mudança

Após os testes não regressivos finalizarem com sucesso, será adicionado um **CARD** referenciando todos os componentes
que dependem de **A**, com motivação de notificar os responsáveis destes serviços para que os alterem.

No momento em que todos os serviços terminarem de alterar, o card será fechado e o pipeline de **A** iniciara a etapa
natural de testes de regressão.

![servico-a](imagens/situacao-pre-producao.png)

Serviço C e D serão iniciados para teste de regressão

> Caso serviço B seja configurado para ser testado independente de seus antecessores,
> ele também será incluido na regressão.

---

Caso a mudança em **A** não seja de contrato, a ordem natural de testar de forma regressiva será inicializada
automaticamente.

![automatização-pre-producao](imagens/atividade-pre-producao-automatizada.png)

Na figura acima podemos analisar a atividade geral para encaminhar para produção uma mudança realizada em um projeto. 