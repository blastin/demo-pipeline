package com.example.demo;

import com.github.tomakehurst.wiremock.client.WireMock;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.cloud.contract.wiremock.AutoConfigureWireMock;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.util.ResourceUtils;

import java.nio.file.Files;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureWireMock(port = 0)
@AutoConfigureMockMvc
@ActiveProfiles("test")
class DemoApplicationTests {

    @Autowired
    private MockMvc mockMvc;

    @Test
    @Sql("pessoa-nome-desenvolvedor.sql")
    @DisplayName("Cenário quando pessoa encontrada pelo id = 100, retorna com nome desenvolvedor")
    void cenarioQuandoPessoaEncontrada() throws Exception {

        mockMvc
                .perform
                        (
                                MockMvcRequestBuilders.get
                                        (
                                                "/pessoas/100"
                                        )
                        )
                .andDo(MockMvcResultHandlers.print())
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.content().json(Files
                        .readString
                                (
                                        ResourceUtils
                                                .getFile("classpath:com/example/demo/pessoa-desenvolvedor-resposta.json")
                                                .toPath()
                                )
                ));

    }

    @Test
    @DisplayName("Cenário quando pessoa não encontrada por id deve retornar mensagem de erro e status 404")
    void cenarioQuandoPessoaNaoEncontrada() throws Exception {

        mockMvc
                .perform
                        (
                                MockMvcRequestBuilders.get
                                        (
                                                "/pessoas/0"
                                        )
                        )
                .andDo(MockMvcResultHandlers.print())
                .andExpect(MockMvcResultMatchers.status().isNotFound())
                .andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.content().json(Files
                        .readString
                                (
                                        ResourceUtils
                                                .getFile("classpath:com/example/demo/pessoa-nao-encontrada-resposta.json")
                                                .toPath()
                                )
                ));

    }

    @Test
    @DisplayName("Cenário quando pessoa sem cadastro em sistema de saude ")
    void cenarioPessoaSemCadastroSistemaSaude() throws Exception {

        WireMock
                .stubFor
                        (
                                WireMock
                                        .get("/planos/pessoa-fisica/".concat("teste"))
                                        .withHeader("Content-Type", WireMock.equalTo(MediaType.APPLICATION_JSON_VALUE))
                                        .willReturn
                                                (
                                                        WireMock
                                                                .status(HttpStatus.NOT_FOUND.value())
                                                )
                        );


        mockMvc
                .perform
                        (
                                MockMvcRequestBuilders.post("/pessoas")
                                        .content
                                                (
                                                        Files
                                                                .readString
                                                                        (
                                                                                ResourceUtils
                                                                                        .getFile("classpath:com/example/demo/pessoa-requisicao.json")
                                                                                        .toPath()
                                                                        )
                                                ).contentType(MediaType.APPLICATION_JSON)
                        )
                .andDo(MockMvcResultHandlers.print())
                .andExpect(MockMvcResultMatchers.status().isUnprocessableEntity())
                .andExpect(MockMvcResultMatchers.content().json(Files
                        .readString
                                (
                                        ResourceUtils
                                                .getFile("classpath:com/example/demo/pessoa-sem-cadastro-sistema-saude.json")
                                                .toPath()
                                )
                ));

    }

    @Test
    @DisplayName("Cenário quando pessoa sem cadastro em sistema de saude ")
    void cenarioPessoaComPlanoDeSaudeInativo() throws Exception {

        WireMock
                .stubFor
                        (
                                WireMock
                                        .get("/planos/pessoa-fisica/".concat("teste"))
                                        .withHeader("Content-Type", WireMock.equalTo(MediaType.APPLICATION_JSON_VALUE))
                                        .willReturn
                                                (
                                                        WireMock
                                                                .status(HttpStatus.OK.value())
                                                                .withHeader("Content-Type", MediaType.APPLICATION_JSON_VALUE)
                                                                .withBody(Files
                                                                        .readString
                                                                                (
                                                                                        ResourceUtils
                                                                                                .getFile("classpath:com/example/demo/situacao-inativo-plano-saude-resposta.json")
                                                                                                .toPath()
                                                                                ))
                                                )
                        );


        mockMvc
                .perform
                        (
                                MockMvcRequestBuilders.post("/pessoas")
                                        .content
                                                (
                                                        Files
                                                                .readString
                                                                        (
                                                                                ResourceUtils
                                                                                        .getFile("classpath:com/example/demo/pessoa-requisicao.json")
                                                                                        .toPath()
                                                                        )
                                                ).contentType(MediaType.APPLICATION_JSON)
                        )
                .andDo(MockMvcResultHandlers.print())
                .andExpect(MockMvcResultMatchers.status().isUnprocessableEntity())
                .andExpect(MockMvcResultMatchers.content().json(Files
                        .readString
                                (
                                        ResourceUtils
                                                .getFile("classpath:com/example/demo/pessoa-com-plano-inativo-resposta.json")
                                                .toPath()
                                )));

    }

    @Test
    @DisplayName("Cenário quando api de saude está indisponivel")
    void cenarioQuandoAPISaudeIndisponivel() throws Exception {

        WireMock
                .stubFor
                        (
                                WireMock
                                        .get("/planos/pessoa-fisica/".concat("teste"))
                                        .withHeader("Content-Type", WireMock.equalTo(MediaType.APPLICATION_JSON_VALUE))
                                        .willReturn
                                                (
                                                        WireMock
                                                                .status(HttpStatus.INTERNAL_SERVER_ERROR.value())
                                                )
                        );


        mockMvc
                .perform
                        (
                                MockMvcRequestBuilders.post("/pessoas")
                                        .content
                                                (
                                                        Files
                                                                .readString
                                                                        (
                                                                                ResourceUtils
                                                                                        .getFile("classpath:com/example/demo/pessoa-requisicao.json")
                                                                                        .toPath()
                                                                        )
                                                ).contentType(MediaType.APPLICATION_JSON)
                        )
                .andDo(MockMvcResultHandlers.print())
                .andExpect(MockMvcResultMatchers.status().isUnprocessableEntity())
                .andExpect(MockMvcResultMatchers.content().json(Files
                        .readString
                                (
                                        ResourceUtils
                                                .getFile("classpath:com/example/demo/sistema-plano-saude-indisponivel.json")
                                                .toPath()
                                )
                ));

    }

    @Test
    @DisplayName("Cenário quando pessoa foi cadastrada")
    void cenarioQuandoPessoaCadastrada() throws Exception {

        WireMock
                .stubFor
                        (
                                WireMock
                                        .get("/planos/pessoa-fisica/".concat("teste"))
                                        .withHeader("Content-Type", WireMock.equalTo(MediaType.APPLICATION_JSON_VALUE))
                                        .willReturn
                                                (
                                                        WireMock
                                                                .status(HttpStatus.CREATED.value())
                                                                .withHeader("Content-Type", MediaType.APPLICATION_JSON_VALUE)
                                                                .withBody(Files
                                                                        .readString
                                                                                (
                                                                                        ResourceUtils
                                                                                                .getFile("classpath:com/example/demo/situacao-ativo-plano-saude-resposta.json")
                                                                                                .toPath()
                                                                                ))
                                                )
                        );

        mockMvc
                .perform
                        (
                                MockMvcRequestBuilders.post("/pessoas")
                                        .content
                                                (
                                                        Files
                                                                .readString
                                                                        (
                                                                                ResourceUtils
                                                                                        .getFile("classpath:com/example/demo/pessoa-requisicao.json")
                                                                                        .toPath()
                                                                        )
                                                ).contentType(MediaType.APPLICATION_JSON)
                        )
                .andDo(MockMvcResultHandlers.print())
                .andExpect(MockMvcResultMatchers.status().isCreated())
                .andExpect(MockMvcResultMatchers.content().string("1"));

    }
}
