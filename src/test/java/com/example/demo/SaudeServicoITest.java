package com.example.demo;

import com.github.tomakehurst.wiremock.client.WireMock;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.cloud.contract.wiremock.AutoConfigureWireMock;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.util.ResourceUtils;

import java.io.IOException;
import java.nio.file.Files;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureWireMock(port = 0)
@ActiveProfiles("test")
class SaudeServicoITest {

    @Autowired
    private SaudeServico saudeServico;

    private static final String NOME = "nome";

    @Test
    void quandoApiDeSaudeIndisponivel() {
        WireMock
                .stubFor
                        (
                                WireMock
                                        .get("/planos/pessoa-fisica/".concat(NOME))
                                        .withHeader("Content-Type", WireMock.equalTo(MediaType.APPLICATION_JSON_VALUE))
                                        .willReturn
                                                (
                                                        WireMock
                                                                .status(HttpStatus.INTERNAL_SERVER_ERROR.value())
                                                )
                        );

        Assertions.assertThrows(FalhaIntegracaoSistemaSaude.class, () -> saudeServico.situacaoPlanoSaude(NOME));

    }

    @Test
    void quandoPlanoDeSaudeNaoContemInformacoesDePessoa() {
        WireMock
                .stubFor
                        (
                                WireMock
                                        .get("/planos/pessoa-fisica/".concat(NOME))
                                        .withHeader("Content-Type", WireMock.equalTo(MediaType.APPLICATION_JSON_VALUE))
                                        .willReturn
                                                (
                                                        WireMock
                                                                .status(HttpStatus.NOT_FOUND.value())
                                                )
                        );

        Assertions.assertThrows(PessoaSemCadastroSistemaSaude.class, () -> saudeServico.situacaoPlanoSaude(NOME));

    }

    @Test
    void quandoPessoaComPlanoDeSaudeNaoAtivado() throws IOException {
        WireMock
                .stubFor
                        (
                                WireMock
                                        .get("/planos/pessoa-fisica/".concat(NOME))
                                        .withHeader("Content-Type", WireMock.equalTo(MediaType.APPLICATION_JSON_VALUE))
                                        .willReturn
                                                (
                                                        WireMock
                                                                .status(HttpStatus.OK.value())
                                                                .withHeader("Content-Type", MediaType.APPLICATION_JSON_VALUE)
                                                                .withBody(Files
                                                                        .readString
                                                                                (
                                                                                        ResourceUtils
                                                                                                .getFile("classpath:com/example/demo/situacao-inativo-plano-saude-resposta.json")
                                                                                                .toPath()
                                                                                ))
                                                )
                        );

        Assertions.assertFalse(saudeServico.situacaoPlanoSaude(NOME));

    }

    @Test
    void quandoPessoaComPlanoDeSaudeAtivado() throws IOException {

        WireMock
                .stubFor
                        (
                                WireMock
                                        .get("/planos/pessoa-fisica/".concat(NOME))
                                        .withHeader("Content-Type", WireMock.equalTo(MediaType.APPLICATION_JSON_VALUE))
                                        .willReturn
                                                (
                                                        WireMock
                                                                .status(HttpStatus.OK.value())
                                                                .withHeader("Content-Type", MediaType.APPLICATION_JSON_VALUE)
                                                                .withBody(Files
                                                                        .readString
                                                                                (
                                                                                        ResourceUtils
                                                                                                .getFile("classpath:com/example/demo/situacao-ativo-plano-saude-resposta.json")
                                                                                                .toPath()
                                                                                ))
                                                )
                        );

        Assertions
                .assertTrue
                        (
                                saudeServico
                                        .situacaoPlanoSaude(NOME)
                        );

    }

}
