package com.example.demo;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

class PessoaServiceTest {

    @Test
    void quandoPessoaComPlanoDeSaudeNaoAtivado() {

        final SaudeServico saudeServico = Mockito.mock(SaudeServico.class);

        Mockito.
                when(saudeServico.situacaoPlanoSaude(Mockito.anyString()))
                .thenReturn(false);

        final PessoaService pessoaService = new PessoaService(null, saudeServico);

        Assertions
                .assertThrows
                        (
                                PessoaComPlanoDeSaudeInativo.class,
                                () -> pessoaService.cadastrar(new Pessoa("oi"))
                        );
    }

    @Test
    void quandoPessoaComPlanoDeSaudeAtivado() {

        final SaudeServico saudeServico = Mockito.mock(SaudeServico.class);

        Mockito.
                when(saudeServico.situacaoPlanoSaude(Mockito.anyString()))
                .thenReturn(true);

        final PessoaRepositorio pessoaRepositorio = Mockito.mock(PessoaRepositorio.class);

        final Pessoa pauloGuedes = new Pessoa("pauloGuedes");

        Mockito
                .when(pessoaRepositorio.save(Mockito.any(Pessoa.class)))
                .thenReturn(pauloGuedes);

        Assertions
                .assertEquals
                        (0L,
                                new PessoaService(pessoaRepositorio, saudeServico)
                                        .cadastrar(pauloGuedes)
                        );

    }
}