package com.example.demo;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@RestControllerAdvice
public class Advice {

    private static final class MensagemError {

        private MensagemError(final RuntimeException exception) {
            this.exception = exception;
        }

        private final RuntimeException exception;

        @SuppressWarnings("unused")
        public String getMensagem() {
            return exception.getMessage();
        }

    }

    @ExceptionHandler(PessoaNaoEncontrada.class)
    public ResponseEntity<MensagemError> pessoaNaoEncontrada(final PessoaNaoEncontrada ex) {
        return ResponseEntity.status(HttpStatus.NOT_FOUND).body(new MensagemError(ex));
    }

    @ExceptionHandler(PessoaComPlanoDeSaudeInativo.class)
    public ResponseEntity<MensagemError> pessoaComPlanoDeSaudeInativo(final PessoaComPlanoDeSaudeInativo ex) {
        return ResponseEntity.status(HttpStatus.UNPROCESSABLE_ENTITY).body(new MensagemError(ex));
    }

    @ExceptionHandler(PessoaSemCadastroSistemaSaude.class)
    public ResponseEntity<MensagemError> pessoasSemCadastroSistemaSaude(final PessoaSemCadastroSistemaSaude ex) {
        return ResponseEntity.status(HttpStatus.UNPROCESSABLE_ENTITY).body(new MensagemError(ex));
    }

    @ExceptionHandler(FalhaIntegracaoSistemaSaude.class)
    public ResponseEntity<MensagemError> falhaIntegracaoSistemaSaude(final FalhaIntegracaoSistemaSaude ex) {
        return ResponseEntity.status(HttpStatus.UNPROCESSABLE_ENTITY).body(new MensagemError(ex));
    }

}
