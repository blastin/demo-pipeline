package com.example.demo;

import feign.Response;

public class FalhaIntegracaoSistemaSaude extends RuntimeException {

    @SuppressWarnings("unused")
    public FalhaIntegracaoSistemaSaude(String s, Response response) {
        // NADA A SER FEITO
    }

    @Override
    public String getMessage() {
        return "Não foi possível completar a operação";
    }
}
