package com.example.demo;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/pessoas")
public class Controle {

    public Controle(final PessoaService pessoaService) {
        this.pessoaService = pessoaService;
    }

    private final PessoaService pessoaService;

    static class PessoaDTO {

        private static PessoaDTO get(final PessoaService pessoaService, final long id) {
            return new PessoaDTO(pessoaService.pessoaPorId(id).getNome());
        }

        private PessoaDTO(final String nome) {
            this.nome = nome;
        }

        private final String nome;

        public String getNome() {
            return nome;
        }
    }

    @GetMapping("/{id}")
    public PessoaDTO pessoaPorId(@PathVariable("id") final long id) {
        return PessoaDTO.get(pessoaService, id);
    }

    static class LongCadastro {

        LongCadastro(final PessoaCadastroDTO pessoaCadastroDTO, final PessoaService pessoaService) {
            this.pessoaCadastroDTO = pessoaCadastroDTO;
            this.pessoaService = pessoaService;
        }

        private final PessoaCadastroDTO pessoaCadastroDTO;

        private final PessoaService pessoaService;

        Long toLong() {
            return pessoaCadastroDTO.cadastrar(pessoaService);
        }

    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Long cadastrar(@RequestBody final PessoaCadastroDTO pessoaCadastroDTO) {
        return new LongCadastro(pessoaCadastroDTO, pessoaService).toLong();
    }
}
