package com.example.demo;

public class SaudeServico {

    public SaudeServico(final SaudeAPI saudeAPI) {
        this.saudeAPI = saudeAPI;
    }

    private final SaudeAPI saudeAPI;

    public boolean situacaoPlanoSaude(final String nome) {
        return saudeAPI
                .situacaoPlanoSaude(nome)
                .orElseThrow(() -> new PessoaSemCadastroSistemaSaude(nome))
                .planoAtivo();
    }

}
