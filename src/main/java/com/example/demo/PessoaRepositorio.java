package com.example.demo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.Optional;

public interface PessoaRepositorio extends JpaRepository<Pessoa, Long> {

    @Query("select p.nome as nome from Pessoa p where p.id = ?1")
    Optional<String> encontrarPessoaPorId(final long id);

}
