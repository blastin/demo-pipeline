package com.example.demo;

import javax.persistence.*;

@Entity
@Table(name = "pessoa")
public class Pessoa {

    protected Pessoa() {
    }

    public Pessoa(final String nome) {
        this.nome = nome;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "pessoa_sequence")
    @SequenceGenerator(name = "pessoa_sequence", sequenceName = "pessoa_seq")
    private long id;

    private String nome;

    public String getNome() {
        return nome;
    }

    public long getId() {
        return id;
    }

}
