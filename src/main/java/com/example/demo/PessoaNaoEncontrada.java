package com.example.demo;

public class PessoaNaoEncontrada extends RuntimeException {

    public PessoaNaoEncontrada(long id) {
        this.id = id;
    }

    private final long id;

    @Override
    public String getMessage() {
        return String.format("Não foi possível encontrar pessoa com id = %d", id);
    }
}
