package com.example.demo;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.Optional;

@FeignClient(name = "servico-de-saude", url = "${servico.saude.url}:${servico.saude.port}", decode404 = true, configuration = FeignConfiguration.class)
public interface SaudeAPI {

    @GetMapping(value = "planos/pessoa-fisica/{nome}", consumes = "application/json")
    Optional<SituacaoPlano> situacaoPlanoSaude(@PathVariable("nome") final String nome);

}
