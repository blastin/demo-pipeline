package com.example.demo;

public class SituacaoPlano {

    public SituacaoPlano() {
        valor = false;
    }

    @SuppressWarnings("unused")
    public void setSituacao(final boolean valor) {
        this.valor = valor;
    }

    private boolean valor;

    public boolean planoAtivo() {
        return valor;
    }

}
