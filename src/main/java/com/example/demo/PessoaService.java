package com.example.demo;

final class PessoaService {

    PessoaService(final PessoaRepositorio pessoaRepositorio, final SaudeServico saudeServico) {
        this.pessoaRepositorio = pessoaRepositorio;
        this.saudeAPI = saudeServico;
    }

    private final PessoaRepositorio pessoaRepositorio;

    private final SaudeServico saudeAPI;

    public PessoaOnly pessoaPorId(final long id) {
        return () ->
                pessoaRepositorio
                        .encontrarPessoaPorId(id)
                        .orElseThrow(() -> new PessoaNaoEncontrada(id));
    }

    public long cadastrar(final Pessoa pessoa) {
        if (saudeAPI.situacaoPlanoSaude(pessoa.getNome())) // Problema arquitetural . Regra de negocio em detalhe [infra]
            return pessoaRepositorio.save(pessoa).getId();
        throw new PessoaComPlanoDeSaudeInativo(pessoa.getNome());
    }

}
