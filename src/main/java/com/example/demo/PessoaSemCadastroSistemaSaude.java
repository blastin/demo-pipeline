package com.example.demo;

public class PessoaSemCadastroSistemaSaude extends RuntimeException {

    public PessoaSemCadastroSistemaSaude(final String nome) {
        this.nome = nome;
    }

    private final String nome;

    @Override
    public String getMessage() {
        return String.format("Não foi possível encontrar em sistema de saude pessoa = %s", nome);
    }

}
