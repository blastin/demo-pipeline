package com.example.demo;

public class PessoaCadastroDTO {

    public void setNome(final String nome) {
        this.nome = nome;
    }

    private String nome;

    public Long cadastrar(final PessoaService pessoaService) {
        return pessoaService.cadastrar(new Pessoa(nome));
    }

}
