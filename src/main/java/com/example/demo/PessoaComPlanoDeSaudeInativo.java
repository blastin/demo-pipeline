package com.example.demo;

public class PessoaComPlanoDeSaudeInativo extends RuntimeException {

    public PessoaComPlanoDeSaudeInativo(String nome) {
        this.nome = nome;
    }

    private final String nome;

    @Override
    public String getMessage() {
        return String.format("Não foi possivel cadastrar pois plano de saude da pessoa = %s, não foi encontrado", nome);
    }
}
