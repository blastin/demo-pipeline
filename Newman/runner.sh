#!/usr/bin/env sh
# Autor : Jefferson Lisboa <lisboa.jeff@gmail.com>
# Objetivo : Disparo de testes para verificar integração com componentes externos
#
run_newman() {
  echo "Iniciando testes automatizados"
  newman run api.json -e Integration.json
}
#
run_server() {
  #
  # Verificando se o comando jq foi instalado
  if test -z "$(command -v jq)"; then
    echo "comando jq não foi encontrado"
    exit 1
  else
    echo "Iniciando mockoon de API's"
    quantidade=$(jq length "${MOCK_CONF}")
    echo "Tentaremos subir no total '${quantidade}' API'S mocks"
    i=0
    while test "$i" -lt "$quantidade"; do
      objeto="$(jq ".[$i].descricao,.[$i].data,.[$i].index,.[$i].porta" "$MOCK_CONF" | tr '\n' '|' | tr -d '"' | sed 's/|$//g')"
      descricao="$(echo "$objeto" | cut -d '|' -f 1)"
      echo "Iniciando '$descricao'"
      data="$(echo "$objeto" | tr -s ' ' | cut -d '|' -f 2)"
      index="$(echo "$objeto" | cut -d '|' -f 3)"
      porta="$(echo "$objeto" | cut -d '|' -f 4)"
      mockoon-cli start --data "$data" --index "$index" --port "$porta"
      i=$((i + 1))
    done
  fi
}
# Verificar a situação de saúde do serviço
# Caso Retorne um status diferente de 200, ocorerrá uma nova tentativa
#
#
health_check() {
  status=0
  while test "$status" -ne 200; do
    echo "[$status] Verificando se o serviço foi inicializado"
    status="$(curl -s -o /dev/null -w "%{http_code}" -k "${HEALTH_CHECK}")"
    sleep "${SLEEP_TIME:=0}"
  done
}
#  Verificando se o comando curl foi instalado no sistema
#
if test -z "$(command -v curl)"; then
  echo "comando curl não foi encontrado"
  exit 1
fi
#
#
#  Verificando se o comando mockoon-cli foi instalado no sistema
#
if test -z "$(command -v mockoon-cli)"; then
  echo "comando mockoon-cli não foi encontrado"
  exit 1
else
  run_server
fi
#
#
echo "serviço de health check = ${HEALTH_CHECK}"
#
#
health_check
#
#
#  Verificando se o comando newman foi instalado no sistema
if test -z "$(command -v newman)"; then
  echo "comando newman não foi encontrado"
  exit 1
else
  run_newman
  # resposta de run_newman será necessária para etapas de integração continua
  newman_resultado="$?"
  #
  exit "${newman_resultado}"
#
fi
