FROM alpine:latest

RUN apk update && apk upgrade && apk add --no-cache openjdk11-jre

RUN adduser servico -D

USER servico

WORKDIR /home/servico

COPY target/*.jar servico.jar

CMD ["java","-jar","-XX:+UseContainerSupport","-XX:+UseG1GC","-Djava.security.egd=file:/dev/./urandom","-Dspring.profiles.active=${ENVIRONMENT_PROFILE}","servico.jar"]
